import java.util.Scanner;

public class Test {
    private static Scanner in = new Scanner(System.in);
    
    public static void Test() {
        int[] znamky = new int[8];
        int znamkyCelkem = 0;
        int pocetZnamek = znamky.length;
        
        for(int i = 0; i < pocetZnamek; i++) {
            int poradovyCislo = i+1;
            
            System.out.print("Zadej " + poradovyCislo + " známku: ");
            znamky[i] = in.nextInt();
            while(znamky[i] < 1 || znamky[i] > 5) {
                System.out.print("Známka musí být v intervalu <1;5>. Zadej novou známku: ");
                znamky[i] = in.nextInt();
            }
            
            znamkyCelkem += znamky[i];
        }
        
        double prumer = (double)znamkyCelkem/pocetZnamek;
        System.out.print("Průměr = " + prumer);
    }
}
